/*

Description
************************************************************************************************************************************
Author:  Mohan Murugesan
Function: Convert XML message to JSON


Version History Log
************************************************************************************************************************************
Date                        <Name>                  Project                        Description of Change

21/07/2017                Mohan Murugesan                  ETM                           Convert XML to JSON
************************************************************************************************************************************

*/

var service = require('service-metadata');
var ReqUid = 'sample';

session.input.readAsXML(function(error, xmlDoc) {
    if (error) {
        console.error(JSON.stringify(error));
        session.reject("Input read error");
        return;
    }
    ///*****Able to read input*****//
    else {
      var converter = require('json-xml-converter');
      var result = converter.toJSON('badgerfish', xmlDoc.item(0));
      
      var inputMsg=result.NagMsg;
      
      var service=require('service-metadata');
      var hdr=require('header-metadata');
      var UUID=session.name('ServiceDetails').getVar('UniqueId');
      hdr.current.set('Content-Type','application/JSON');
      hdr.current.set('x-messageId',UUID);
      
      
      var outputResponse=new Object ();      
      /*var NagMsg = new Object ();
      outputResponse.NagMsg = NagMsg;
      
      
      
      var GblMsgHdr = new Object ();
      outputResponse.GblMsgHdr = GblMsgHdr;
      var HdrInfo = new Object ();
      HdrInfo.HdrVersNbrId=inputMsg.GblMsgHdr.HdrInfo.HdrVersNbrId.$;
      outputResponse.HdrInfo = HdrInfo;
      var MsgResp = new Object ();
      MsgResp.RespCode = inputMsg.GblMsgHdr.MsgResp.RespCode.$;
      MsgResp.RsnCode = inputMsg.GblMsgHdr.MsgResp.RsnCode.$;
      MsgResp.RsnDesc = inputMsg.GblMsgHdr.MsgResp.RsnDesc.$;
      MsgResp.SnsDataDesc = inputMsg.GblMsgHdr.MsgResp.SnsDataDesc.$;
      MsgResp.RtSnsDataDesc = inputMsg.GblMsgHdr.MsgResp.RtSnsDataDesc.$;
      outputResponse.MsgResp=MsgResp;
      
      
      var MsgIdn = new Object ();
      outputResponse.MsgIdn=MsgIdn;
      
      var SrcRoutInfo = new Object ();
      SrcRoutInfo.Chnl=inputMsg.GblMsgHdr.MsgIdn.SrcRoutInfo.Chnl.$;
      SrcRoutInfo.SubChnl=inputMsg.GblMsgHdr.MsgIdn.SrcRoutInfo.SubChnl.$;
      outputResponse.SrcRoutInfo=SrcRoutInfo;
      
      
      var RoutSrc = new Object ();
      RoutSrc.LogSrvcId=inputMsg.GblMsgHdr.MsgIdn.SrcRoutInfo.RoutSrc.LogSrvcId.$;
      RoutSrc.SrvcVersNbrId=inputMsg.GblMsgHdr.MsgIdn.SrcRoutInfo.RoutSrc.SrvcVersNbrId.$;
      outputResponse.RoutSrc=RoutSrc;
      
      
      var RoutDest = new Object();
      RoutDest.LogSrvcId = inputMsg.GblMsgHdr.MsgIdn.SrcRoutInfo.RoutDest.LogSrvcId.$;
      RoutDest.SrvcVersNbrId = inputMsg.GblMsgHdr.MsgIdn.SrcRoutInfo.RoutDest.SrvcVersNbrId.$;
      outputResponse.RoutDest=RoutDest;
      
      var SIGNONMSGSRQV1 = new Object ();
      outputResponse.SIGNONMSGSRQV1=SIGNONMSGSRQV1;
      
      var SONRQ = new Object ();
      SONRQ.CHANNELID = inputMsg.SIGNONMSGSRQV1.SONRQ.CHANNELID.$;
      SONRQ.USERID = inputMsg.SIGNONMSGSRQV1.SONRQ.USERID.$;
      SONRQ.SUBUSERID = inputMsg.SIGNONMSGSRQV1.SONRQ.SUBUSERID.$;
      SONRQ.USERPASS = inputMsg.SIGNONMSGSRQV1.SONRQ.USERPASS.$;
      SONRQ.SRVRTID = inputMsg.SIGNONMSGSRQV1.SONRQ.SRVRTID.$;
      outputResponse.SONRQ=SONRQ;*/
      
      
      var INQACCTOWNRSREQ = new Object ();
      outputResponse.INQACCTOWNRSREQ=INQACCTOWNRSREQ;
      
      var BANKACCTFROM = new Object ();
      BANKACCTFROM.BANKID = inputMsg.INQACCTOWNRSREQ.BANKACCTFROM.BANKID.$;
      BANKACCTFROM.BRANCHID = inputMsg.INQACCTOWNRSREQ.BANKACCTFROM.BRANCHID.$;
      BANKACCTFROM.ACCTID = inputMsg.INQACCTOWNRSREQ.BANKACCTFROM.ACCTID.$;
      BANKACCTFROM.ACCTTYPE = inputMsg.INQACCTOWNRSREQ.BANKACCTFROM.ACCTTYPE.$;
      outputResponse.BANKACCTFROM=BANKACCTFROM;
      
      console.debug("TEST"+JSON.stringify(outputResponse));
      session.output.write(outputResponse);
      
      }
});
