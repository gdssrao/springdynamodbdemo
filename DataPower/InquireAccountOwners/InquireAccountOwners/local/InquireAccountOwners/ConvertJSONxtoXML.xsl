<?xml version="1.0" encoding="UTF-8"?>
<!--

Description
************************************************************************************************************************************
Author:  Mohan Murugesan
Function: Convert JSONx message to XML


Version History Log
************************************************************************************************************************************
Date                        <Name>                  Project                        Description of Change

21/07/2017                Mohan Murugesan           GCS POC                        Convert JSONx to XML
************************************************************************************************************************************

-->

<xsl:stylesheet version="1.0"  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"  xmlns:dp="http://www.datapower.com/extensions"  extension-element-prefixes="dp">
  <xsl:output omit-xml-declaration="yes" />
  <xsl:template match="/">
      <xsl:element name="INQACCTOWNRSRSP">
        <xsl:element name="ACCTNM">
          <xsl:value-of select="*[local-name()='object']/*[local-name()='object'][@name='INQACCTOWNRSRSP']/*[local-name()='string'][@name='ACCTNM']"/>
        </xsl:element>
        <xsl:element name="ACCTSHRTNM">
          <xsl:value-of select="*[local-name()='object']/*[local-name()='object'][@name='INQACCTOWNRSRSP']/*[local-name()='string'][@name='ACCTSHRTNM']"/>
        </xsl:element>
        <xsl:element name="DTACCTCLSE">
          <xsl:value-of select="*[local-name()='object']/*[local-name()='object'][@name='INQACCTOWNRSRSP']/*[local-name()='string'][@name='DTACCTCLSE']"/>
        </xsl:element>
        <xsl:element name="ACCTOWNRDTLS">
            <xsl:element name="CNSMRID">
              <xsl:value-of select="*[local-name()='object']/*[local-name()='object'][@name='INQACCTOWNRSRSP']/*[local-name()='object'][@name='ACCTOWNRDTLS']/*[local-name()='string'][@name='CNSMRID']"/>
            </xsl:element>
            <xsl:element name="OWNRCDE">
              <xsl:value-of select="*[local-name()='object']/*[local-name()='object'][@name='INQACCTOWNRSRSP']/*[local-name()='object'][@name='ACCTOWNRDTLS']/*[local-name()='string'][@name='OWNRCDE']"/>
            </xsl:element>
            <xsl:element name="CONM">
              <xsl:value-of select="*[local-name()='object']/*[local-name()='object'][@name='INQACCTOWNRSRSP']/*[local-name()='object'][@name='ACCTOWNRDTLS']/*[local-name()='string'][@name='CONM']"/>
            </xsl:element>
            <xsl:element name="CNSMRNM">
              <xsl:element name="CNSMRLASTNM">
                <xsl:value-of select="*[local-name()='object']/*[local-name()='object'][@name='INQACCTOWNRSRSP']/*[local-name()='object'][@name='ACCTOWNRDTLS']/*[local-name()='object'][@name='CNSMRNM']/*[local-name()='string'][@name='CNSMRLASTNM']"/>
              </xsl:element>
              <xsl:element name="CNSMRFRSTNM">
                <xsl:value-of select="*[local-name()='object']/*[local-name()='object'][@name='INQACCTOWNRSRSP']/*[local-name()='object'][@name='ACCTOWNRDTLS']/*[local-name()='object'][@name='CNSMRNM']/*[local-name()='string'][@name='CNSMRLASTNM']"/>
              </xsl:element>
              <xsl:element name="CNSMRMDLNM">
                <xsl:value-of select="*[local-name()='object']/*[local-name()='object'][@name='INQACCTOWNRSRSP']/*[local-name()='object'][@name='ACCTOWNRDTLS']/*[local-name()='object'][@name='CNSMRNM']/*[local-name()='string'][@name='CNSMRMDLNM']"/>
              </xsl:element>
              <xsl:element name="CNSMRSCNDMDLNM">
                <xsl:value-of select="*[local-name()='object']/*[local-name()='object'][@name='INQACCTOWNRSRSP']/*[local-name()='object'][@name='ACCTOWNRDTLS']/*[local-name()='object'][@name='CNSMRNM']/*[local-name()='string'][@name='CNSMRSCNDMDLNM']"/>
              </xsl:element>
              <xsl:element name="CNSMRNMTTL">
                <xsl:value-of select="*[local-name()='object']/*[local-name()='object'][@name='INQACCTOWNRSRSP']/*[local-name()='object'][@name='ACCTOWNRDTLS']/*[local-name()='object'][@name='CNSMRNM']/*[local-name()='string'][@name='CNSMRNMTTL']"/>
              </xsl:element>
            </xsl:element>
        </xsl:element>
        <xsl:element name="ACCTRELNDTLS">
          <xsl:element name="CNSMRID">
            <xsl:value-of select="*[local-name()='object']/*[local-name()='object'][@name='INQACCTOWNRSRSP']/*[local-name()='object'][@name='ACCTRELNDTLS']/*[local-name()='string'][@name='CNSMRID']"/>
          </xsl:element>
          <xsl:element name="RELNCDE">
            <xsl:value-of select="*[local-name()='object']/*[local-name()='object'][@name='INQACCTOWNRSRSP']/*[local-name()='object'][@name='ACCTRELNDTLS']/*[local-name()='string'][@name='RELNCDE']"/>
          </xsl:element>
          <xsl:element name="CONM">
            <xsl:value-of select="*[local-name()='object']/*[local-name()='object'][@name='INQACCTOWNRSRSP']/*[local-name()='object'][@name='ACCTRELNDTLS']/*[local-name()='string'][@name='CONM']"/>
          </xsl:element>
          <xsl:element name="CNSMRNM">
            <xsl:element name="CNSMRLASTNM">
              <xsl:value-of select="*[local-name()='object']/*[local-name()='object'][@name='INQACCTOWNRSRSP']/*[local-name()='object'][@name='ACCTRELNDTLS']/*[local-name()='object'][@name='CNSMRNM']/*[local-name()='string'][@name='CNSMRLASTNM']"/>
            </xsl:element>
            <xsl:element name="CNSMRFRSTNM">
              <xsl:value-of select="*[local-name()='object']/*[local-name()='object'][@name='INQACCTOWNRSRSP']/*[local-name()='object'][@name='ACCTRELNDTLS']/*[local-name()='object'][@name='CNSMRNM']/*[local-name()='string'][@name='CNSMRFRSTNM']"/>
            </xsl:element>
            <xsl:element name="CNSMRMDLNM">
              <xsl:value-of select="*[local-name()='object']/*[local-name()='object'][@name='INQACCTOWNRSRSP']/*[local-name()='object'][@name='ACCTRELNDTLS']/*[local-name()='object'][@name='CNSMRNM']/*[local-name()='string'][@name='CNSMRMDLNM']"/>
            </xsl:element>
            <xsl:element name="CNSMRSCNDMDLNM">
              <xsl:value-of select="*[local-name()='object']/*[local-name()='object'][@name='INQACCTOWNRSRSP']/*[local-name()='object'][@name='ACCTRELNDTLS']/*[local-name()='object'][@name='CNSMRNM']/*[local-name()='string'][@name='CNSMRSCNDMDLNM']"/>
            </xsl:element>
            <xsl:element name="CNSMRNMTTL">
              <xsl:value-of select="*[local-name()='object']/*[local-name()='object'][@name='INQACCTOWNRSRSP']/*[local-name()='object'][@name='ACCTRELNDTLS']/*[local-name()='object'][@name='CNSMRNM']/*[local-name()='string'][@name='CNSMRNMTTL']"/>
            </xsl:element>
          </xsl:element>
        </xsl:element>
      </xsl:element>
  </xsl:template>
</xsl:stylesheet>
