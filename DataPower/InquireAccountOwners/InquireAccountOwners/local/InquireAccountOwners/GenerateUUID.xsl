<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"  xmlns:dp="http://www.datapower.com/extensions"  extension-element-prefixes="dp">
  <xsl:output omit-xml-declaration="yes" />
  <xsl:template match="/">
    <xsl:variable name="UUID" select="dp:generate-uuid()"/>
    <dp:set-variable name="'var://context/ServiceDetails/UniqueId'" value="string($UUID)"/>
    <xsl:variable name="originalMQMD" select="dp:parse(dp:request('MQMD'))"/>
    <dp:set-variable name="'var://context/Header/OriginalMQMD'" value="$originalMQMD"/>
  </xsl:template>
</xsl:stylesheet>
