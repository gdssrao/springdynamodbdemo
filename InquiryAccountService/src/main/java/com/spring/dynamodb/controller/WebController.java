package com.spring.dynamodb.controller;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.spring.dynamodb.model.InquiryAccount;
import com.spring.dynamodb.repo.InquiryAccountRepository;

@RestController
@RequestMapping("/inquiry")
public class WebController {

	@Autowired
	InquiryAccountRepository repository;

	@RequestMapping("/delete")
	public String delete() {
		repository.deleteAll();
		return "Done";
	}

	@RequestMapping(value="/updateInquiry",method=RequestMethod.POST)
	public String save(@RequestBody String inputbody, @RequestHeader("X-messageId") String msgId) {
		String result="{\r\n" + 
				"    \"INQACCTOWNRSRSP\": {\r\n" + 
				"      \"ACCTNM\": \"Text\",\r\n" + 
				"      \"ACCTSHRTNM\": \"Text\",\r\n" + 
				"      \"DTACCTCLSE\": \"Text\",\r\n" + 
				"      \"ACCTOWNRDTLS\":\r\n" + 
				"        {\r\n" + 
				"          \"CNSMRID\": \"Text\",\r\n" + 
				"          \"OWNRCDE\": \"Text\",\r\n" + 
				"          \"CONM\": \"Text\",\r\n" + 
				"          \"CNSMRNM\": {\r\n" + 
				"            \"CNSMRLASTNM\": \"Text\",\r\n" + 
				"            \"CNSMRFRSTNM\": \"Text\",\r\n" + 
				"            \"CNSMRMDLNM\": \"Text\",\r\n" + 
				"            \"CNSMRSCNDMDLNM\": \"Text\",\r\n" + 
				"            \"CNSMRNMTTL\": \"Text\"\r\n" + 
				"          }\r\n" + 
				"        },\r\n" + 
				"      \"ACCTRELNDTLS\":\r\n" + 
				"        {\r\n" + 
				"          \"CNSMRID\": \"Text\",\r\n" + 
				"          \"RELNCDE\": \"Text\",\r\n" + 
				"          \"CONM\": \"Text\",\r\n" + 
				"          \"CNSMRNM\": {\r\n" + 
				"            \"CNSMRLASTNM\": \"Text\",\r\n" + 
				"            \"CNSMRFRSTNM\": \"Text\",\r\n" + 
				"            \"CNSMRMDLNM\": \"Text\",\r\n" + 
				"            \"CNSMRSCNDMDLNM\": \"Text\",\r\n" + 
				"            \"CNSMRNMTTL\": \"Text\"\r\n" + 
				"          }\r\n" + 
				"        }\r\n" + 
				"    }\r\n" + 
				"}\r\n" + 
				"";
		// save a single Inquiry
/*		ClassLoader classLoader = getClass().getClassLoader();
		StringBuilder result = new StringBuilder("");
		File file = new File(classLoader.getResource("inquiryresponse.json").getFile());
		try (Scanner scanner = new Scanner(file)) {

			while (scanner.hasNextLine()) {
				String line = scanner.nextLine();
				result.append(line).append("\n");
			}

			scanner.close();

		} catch (IOException e) {
			e.printStackTrace();
			result.append(e.getMessage());
		}
			
		*/
		repository.save(new InquiryAccount(msgId,inputbody,result));
		// save a list of Customers
		
		return result.toString();
	}

	@RequestMapping("/findall")
	public String findAll() {
		String result = "";
		Iterable<InquiryAccount> inquiry = repository.findAll();

		for (InquiryAccount inq : inquiry) {
			result += inq.toString();
		}

		return result;
	}

	@RequestMapping("/findbyid")
	public String findById(@RequestHeader("X-messageId") String msgId) {
		String result = "";
		if(repository.findOne(msgId)!=null)
			result = repository.findOne(msgId).toString();
		else
			result="No Record Found";
		return result;
	}

}
