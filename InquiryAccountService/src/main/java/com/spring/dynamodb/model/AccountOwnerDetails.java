package com.spring.dynamodb.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class AccountOwnerDetails {
@JsonProperty("CNSMRID")
private String consumerId;
@JsonProperty("OWNRCDE")
private String ownerCode;
@JsonProperty("CONM")
private String companyName;
@JsonProperty("CNSMRNM")
private ConsumerNames consumername;

public String getConsumerId() {
	return consumerId;
}
public void setConsumerId(String consumerId) {
	this.consumerId = consumerId;
}
public String getOwnerCode() {
	return ownerCode;
}
public void setOwnerCode(String ownerCode) {
	this.ownerCode = ownerCode;
}
public String getCompanyName() {
	return companyName;
}
public void setCompanyName(String companyName) {
	this.companyName = companyName;
}
public ConsumerNames getConsumername() {
	return consumername;
}
public void setConsumername(ConsumerNames consumername) {
	this.consumername = consumername;
}

}
