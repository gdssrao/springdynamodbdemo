package com.spring.dynamodb.model;


import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;

@DynamoDBTable(tableName = "InquiryAccount")
public class InquiryAccount {

	@DynamoDBHashKey(attributeName="Id")
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	@DynamoDBAttribute(attributeName="body")
	public String getBody() {
		return body;
	}
	public void setBody(String body) {
		this.body = body;
	}
	
	public String toString() {
		return inqAccountResponses;
	}
	
	public InquiryAccount() {
		
	}
	
	public InquiryAccount(String id,String body,String inq) {
		this.id=id;
		this.body=body;
		this.inqAccountResponses=inq;
	}
	
	private String id;
	private String body;
	private String inqAccountResponses;
	@DynamoDBAttribute(attributeName="inqAccountResponses")
	public String getInqAccountResponses() {
		return inqAccountResponses;
	}
	public void setInqAccountResponses(String inqAccountResponses) {
		this.inqAccountResponses = inqAccountResponses;
	}
	

	
}
