package com.spring.dynamodb.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class InquiryAccountOwnerResponse {
@JsonProperty("ACCTNM")
private String accountName;
@JsonProperty("ACCTSHRTNM")
private String accountShortName;
@JsonProperty("DTACCTCLSE")
private String dateAccountClose;
@JsonProperty("ACCTOWNRDTLS")
private AccountOwnerDetails accountOwnerDetail;

public AccountOwnerDetails getAccountOwnerDetail() {
	return accountOwnerDetail;
}
public void setAccountOwnerDetail(AccountOwnerDetails accountOwnerDetail) {
	this.accountOwnerDetail = accountOwnerDetail;
}
public String getAccountName() {
	return accountName;
}
public void setAccountName(String accountName) {
	this.accountName = accountName;
}
public String getAccountShortName() {
	return accountShortName;
}
public void setAccountShortName(String accountShortName) {
	this.accountShortName = accountShortName;
}
public String getDateAccountClose() {
	return dateAccountClose;
}
public void setDateAccountClose(String dateAccountClose) {
	this.dateAccountClose = dateAccountClose;
}

}
