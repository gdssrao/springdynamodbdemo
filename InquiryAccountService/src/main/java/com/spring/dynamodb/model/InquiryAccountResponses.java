package com.spring.dynamodb.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class InquiryAccountResponses {
@JsonProperty("INQACCTOWNRSRSP")
private InquiryAccountOwnerResponse inqOwnerResponse;

public InquiryAccountOwnerResponse getInqOwnerResponse() {
	return inqOwnerResponse;
}

public void setInqOwnerResponse(InquiryAccountOwnerResponse inqOwnerResponse) {
	this.inqOwnerResponse = inqOwnerResponse;
}

}
