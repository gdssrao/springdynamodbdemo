package com.spring.dynamodb.controller;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.spring.dynamodb.model.InquiryAccount;
import com.spring.dynamodb.model.InquiryCASBAccountResponses;
import com.spring.dynamodb.model.InquiryDPAccountOwnerResponse;
import com.spring.dynamodb.model.InquiryDPAccountOwnerResponses;
import com.spring.dynamodb.model.InquiryMEAccountResponses;
import com.spring.dynamodb.repo.InquiryAccountRepository;

@RestController
@RequestMapping("/api/inquiry")
public class WebController {

	@Autowired
	InquiryAccountRepository repository;

	@RequestMapping("/delete")
	public String delete() {
		repository.deleteAll();
		return "Done";
	}
	
	@Value("${ec2.datapower.me.endpoint}")
	private String meEndpoint;

	@Value("${ec2.datapower.casb.endpoint}")
	private String casbEndpoint;

	@RequestMapping(value="/account",method=RequestMethod.POST)
	public InquiryDPAccountOwnerResponses update(@RequestBody String inputbody, @RequestHeader("X-Request-Id") String msgId) {
		
		RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = createHttpHeaders(msgId);
        
        HttpEntity<String> entity = new HttpEntity<String>(inputbody,headers);
        InquiryMEAccountResponses meresult = restTemplate.postForObject(meEndpoint, entity, InquiryMEAccountResponses.class);
        InquiryCASBAccountResponses casbresult = restTemplate.postForObject(casbEndpoint, entity, InquiryCASBAccountResponses.class);
        
        InquiryDPAccountOwnerResponses finalresult=new InquiryDPAccountOwnerResponses();
        InquiryDPAccountOwnerResponse result=new InquiryDPAccountOwnerResponse();
        
        result.setAccontname(casbresult.getInqOwnerResponse().getAccountName());
        result.setAccountshortname(casbresult.getInqOwnerResponse().getAccountShortName());
        result.setDateaccountclose(casbresult.getInqOwnerResponse().getDateAccountClose());
        result.setAccountownerdetails(casbresult.getInqOwnerResponse().getAccountOwnerDetail());
        result.setAccoutrelationaldetails(meresult.getInqOwnerResponse().getInqOwnerResponse());
       
        finalresult.setInqOwnerResponse(result);        

    	repository.save(new InquiryAccount(msgId,inputbody,meresult,casbresult,finalresult));	
			
		return finalresult;
	}
/*
	@RequestMapping("/accounts")
	public String findAll() {
		ObjectMapper mapper = new ObjectMapper();
		Iterable<InquiryAccount> inquiry = repository.findAll();
		InquiryAccount result = new InquiryAccount();
		List<InquiryDPAccountOwnerResponses> dpResponseList;
		InquiryDPAccountOwnerResponses dpResponse=null;
		InquiryDPAccountOwnerResponsesList finalList=null;
		for (InquiryAccount dpresponse : inquiry) {
			dpResponse = mapper.readValue(dpresponse.getFinalresult(), InquiryDPAccountOwnerResponses.class);
			dpResponseList.add(dpResponse);
		}
		finalList.setInqOwnerResponse(inqOwnerResponse);
		return result;
	}*/

	@RequestMapping("/query")
	public InquiryDPAccountOwnerResponses findById(@RequestHeader("X-Request-Id") String msgId) {
		InquiryAccount result = new InquiryAccount();
		if(repository.findOne(msgId)!=null)
			result = repository.findOne(msgId);

		ObjectMapper mapper = new ObjectMapper();
		
		InquiryDPAccountOwnerResponses dpResponse=null;
		try {
			dpResponse = mapper.readValue(result.getFinalresult(), InquiryDPAccountOwnerResponses.class);
		} catch (JsonParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return dpResponse;
	}

	private HttpHeaders createHttpHeaders(String msgId)
	{
	    HttpHeaders headers = new HttpHeaders();
	    headers.setContentType(MediaType.APPLICATION_JSON);
	    headers.add("X-Request-Id", msgId);
	    return headers;
	}
}
