package com.spring.dynamodb.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class AccountRelationalDetails {
	@JsonProperty("CNSMRID")
	private String consumerId;
	@JsonProperty("RELNCDE")
	private String relationalName;
	@JsonProperty("CONM")
	private String companyName;
	@JsonProperty("CNSMRNM")
	private ConsumerNames consumerName;
	
	public String getConsumerId() {
		return consumerId;
	}
	public void setConsumerId(String consumerId) {
		this.consumerId = consumerId;
	}
	public String getOwnerCode() {
		return relationalName;
	}
	public void setOwnerCode(String ownerCode) {
		this.relationalName = ownerCode;
	}
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public ConsumerNames getConsumerName() {
		return consumerName;
	}
	public void setConsumerName(ConsumerNames consumerName) {
		this.consumerName = consumerName;
	}
}
