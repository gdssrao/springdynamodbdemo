package com.spring.dynamodb.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class BankAccount {
	@JsonProperty("BANKACCTFROM")
	private BankAccountFrom bankAccountFrom;

	public BankAccountFrom getBankAccountFrom() {
		return bankAccountFrom;
	}

	public void setBankAccountFrom(BankAccountFrom bankAccountFrom) {
		this.bankAccountFrom = bankAccountFrom;
	}
	

}
