package com.spring.dynamodb.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class BankAccountFrom {
    public String getBankId() {
		return bankId;
	}
	public void setBankId(String bankId) {
		this.bankId = bankId;
	}
	public String getBranchId() {
		return branchId;
	}
	public void setBranchId(String branchId) {
		this.branchId = branchId;
	}
	public String getAccountId() {
		return accountId;
	}
	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}
	public String getAccountType() {
		return accountType;
	}
	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}
	@JsonProperty("BANKID")
    private String bankId;
    @JsonProperty("BRANCHID")
    private String branchId;
    @JsonProperty("ACCTID")
    private String accountId;
    @JsonProperty("ACCTTYPE")
    private String accountType;
    
}
