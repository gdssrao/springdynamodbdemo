package com.spring.dynamodb.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ConsumerNames {
@JsonProperty("CNSMRLASTNM")
private String lastName;
@JsonProperty("CNSMRFRSTNM")
private String firstName;
@JsonProperty("CNSMRMDLNM")
private String middleName;
@JsonProperty("CNSMRSCNDMDLNM")
private String secondMiddleName;
@JsonProperty("CNSMRNMTTL")
private String title;
public String getLastName() {
	return lastName;
}
public void setLastName(String lastName) {
	this.lastName = lastName;
}
public String getFirstName() {
	return firstName;
}
public void setFirstName(String firstName) {
	this.firstName = firstName;
}
public String getMiddleName() {
	return middleName;
}
public void setMiddleName(String middleName) {
	this.middleName = middleName;
}
public String getSecondMiddleName() {
	return secondMiddleName;
}
public void setSecondMiddleName(String secondMiddleName) {
	this.secondMiddleName = secondMiddleName;
}
public String getTitle() {
	return title;
}
public void setTitle(String title) {
	this.title = title;
}

}
