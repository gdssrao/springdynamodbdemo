package com.spring.dynamodb.model;


import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@DynamoDBTable(tableName = "InquiryAccount")
public class InquiryAccount {

	@DynamoDBHashKey(attributeName="Id")
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}

	public InquiryAccount() {
		
	}
	
	public InquiryAccount(String id,String body,InquiryMEAccountResponses meresponse,InquiryCASBAccountResponses casbresponse,InquiryDPAccountOwnerResponses finalresult) {
		this.id=id;
		this.body=body;
		ObjectMapper mapper=new ObjectMapper();
		try {
			this.meresponse=mapper.writeValueAsString(meresponse);
			this.casbresponse=mapper.writeValueAsString(casbresponse);
			this.finalresult=mapper.writeValueAsString(finalresult);
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//this.meresponse=meresponse;
	//	this.casbresponse=casbresponse;
		//this.finalresult=finalresult;
	}
	
	@DynamoDBAttribute(attributeName="InquiryUpdateRequest")
	public String getBody() {
		
		return body;
	}
	public void setBody(String body) {
		this.body = body;
	}

	private String id;
	private String body;
	private String meresponse;
	private String casbresponse;
	private String finalresult;
	
	@DynamoDBAttribute(attributeName="InquiryUpdateResponse")
	public String getFinalresult() {
		return finalresult;
	}
	public void setFinalresult(String finalresult) {
		this.finalresult = finalresult;
	}
	@DynamoDBAttribute(attributeName="MEResponse")
	public String getMeresponse() {
		return meresponse;
	}
	public void setMeresponse(String meresponse) {
		this.meresponse = meresponse;
	}
	@DynamoDBAttribute(attributeName="CASBResponse")
	public String getCasbresponse() {
		return casbresponse;
	}
	public void setCasbresponse(String casbresponse) {
		this.casbresponse = casbresponse;
	}

	
}
