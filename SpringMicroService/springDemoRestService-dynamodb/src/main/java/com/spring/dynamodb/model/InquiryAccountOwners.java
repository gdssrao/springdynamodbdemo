package com.spring.dynamodb.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class InquiryAccountOwners {

@JsonProperty("INQACCTOWNRSREQ")
private BankAccount bankAccount;

public BankAccount getBankAccount() {
	return bankAccount;
}

public void setBankAccountFrom(BankAccount bankAccount) {
	this.bankAccount = bankAccount;
}


}
