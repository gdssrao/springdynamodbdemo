package com.spring.dynamodb.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class InquiryCASBAccountResponses {
@JsonProperty("INQACCTOWNRSRSP")
private InquiryAccountOwnerResponse inqOwnerResponse;

public InquiryAccountOwnerResponse getInqOwnerResponse() {
	return inqOwnerResponse;
}

public void setInqOwnerResponse(InquiryAccountOwnerResponse inqOwnerResponse) {
	this.inqOwnerResponse = inqOwnerResponse;
}

}
