package com.spring.dynamodb.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class InquiryDPAccountOwnerResponse {
	@JsonProperty("ACCTNM")
private String accontname; 
	@JsonProperty("ACCTSHRTNM")
private String accountshortname;
	@JsonProperty("DTACCTCLSE")
private String dateaccountclose;
	@JsonProperty("ACCTOWNRDTLS")
private AccountOwnerDetails accountownerdetails;
	@JsonProperty("ACCTRELNDTLS")
private AccountRelationalDetails accoutrelationaldetails;
public String getAccontname() {
	return accontname;
}
public void setAccontname(String accontname) {
	this.accontname = accontname;
}
public String getAccountshortname() {
	return accountshortname;
}
public void setAccountshortname(String accountshortname) {
	this.accountshortname = accountshortname;
}
public String getDateaccountclose() {
	return dateaccountclose;
}
public void setDateaccountclose(String dateaccountclose) {
	this.dateaccountclose = dateaccountclose;
}
public AccountOwnerDetails getAccountownerdetails() {
	return accountownerdetails;
}
public void setAccountownerdetails(AccountOwnerDetails accountownerdetails) {
	this.accountownerdetails = accountownerdetails;
}
public AccountRelationalDetails getAccoutrelationaldetails() {
	return accoutrelationaldetails;
}
public void setAccoutrelationaldetails(AccountRelationalDetails accoutrelationaldetails) {
	this.accoutrelationaldetails = accoutrelationaldetails;
}

}
