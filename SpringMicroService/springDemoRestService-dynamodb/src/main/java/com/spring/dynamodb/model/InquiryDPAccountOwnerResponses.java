package com.spring.dynamodb.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class InquiryDPAccountOwnerResponses {
	@JsonProperty("INQACCTOWNRSRSP")
	private InquiryDPAccountOwnerResponse inqOwnerResponse;

	public InquiryDPAccountOwnerResponse getInqOwnerResponse() {
		return inqOwnerResponse;
	}

	public void setInqOwnerResponse(InquiryDPAccountOwnerResponse inqOwnerResponse) {
		this.inqOwnerResponse = inqOwnerResponse;
	}
}
