package com.spring.dynamodb.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class InquiryMEAccountResponse {
@JsonProperty("ACCTRELNDTLS")
private AccountRelationalDetails inqOwnerResponse;

public AccountRelationalDetails getInqOwnerResponse() {
	return inqOwnerResponse;
}

public void setInqOwnerResponse(AccountRelationalDetails inqOwnerResponse) {
	this.inqOwnerResponse = inqOwnerResponse;
}

}
