package com.spring.dynamodb.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class InquiryMEAccountResponses {
@JsonProperty("INQACCTOWNRSRSP")
private InquiryMEAccountResponse inqOwnerResponse;

public InquiryMEAccountResponse getInqOwnerResponse() {
	return inqOwnerResponse;
}

public void setInqOwnerResponse(InquiryMEAccountResponse inqOwnerResponse) {
	this.inqOwnerResponse = inqOwnerResponse;
}

}
