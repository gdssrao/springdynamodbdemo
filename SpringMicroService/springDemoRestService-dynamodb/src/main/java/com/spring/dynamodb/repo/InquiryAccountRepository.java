package com.spring.dynamodb.repo;

import org.socialsignin.spring.data.dynamodb.repository.EnableScan;
import org.springframework.data.repository.CrudRepository;

import com.spring.dynamodb.model.InquiryAccount;

@EnableScan
public interface InquiryAccountRepository extends CrudRepository<InquiryAccount, String> {

	 
}

